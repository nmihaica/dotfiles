## Dotfiles

Lurk around and `wget` if you like something.
To get bash-git-prompt

`git clone https://github.com/magicmonty/bash-git-prompt`

## rxvt-unicode-256 
![Urxvt](shots/urxvt.png)

## gvim
![gvim](shots/gvim.png)
