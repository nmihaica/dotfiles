if has("gui_running")
  if has("gui_gtk2")
    set guifont=Hack\ 10
  elseif has("gui_macvim")
    set guifont=Hack\ Regular:h14
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif

" Toggle relative number when in normal mode, but switch back in insert
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

"nnoremap <C-d> :sh<cr>

set tabstop=2 
set shiftwidth=2 
set expandtab
set linespace=2

"gitguter
"GitGutterEnable
"default update time is 4000
"set updatetime = 100

"disable .swp
set noswapfile

".vimrc
"map <c-f> :call JsBeautify()<cr>
" or
"autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for json
"autocmd FileType json noremap <buffer> <c-f> :call JsonBeautify()<cr>
" for jsx
"autocmd FileType jsx noremap <buffer> <c-f> :call JsxBeautify()<cr>
" for html
"autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" for css or scss
"autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>

" CTRL + S to save
noremap <silent> <C-S>                :update<CR>
vnoremap <silent> <C-S>               <C-C>:update<CR>
inoremap <silent> <C-S>               <C-O>:update<CR>

"noremap <F3> :Autoformat<CR>
inoremap jk <Esc>`^
" Nerdtree
map <C-a> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1
" Vim emmet workaround
let g:user_emmet_expandabbr_key='<Tab>'
imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")


call plug#begin('~/.vim/plugged')

"Plug 'posva/vim-vue'
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
"Plug 'pangloss/vim-javascript'
"Plug 'mxw/vim-jsx'
"Plug 'tomasiser/vim-code-dark'
"Plug 'owickstrom/vim-colors-paramount'
"Plug 'rakr/vim-one'
"Plug 'Chiel92/vim-autoformat'
"Plug 'terryma/vim-multiple-cursors'
Plug 'scrooloose/nerdtree'
"Plug 'pangloss/vim-javascript'
"Plug 'flazz/vim-colorschemes'
"Plug 'mattn/emmet-vim'
"Plug 'posva/vim-vue'
"Plug 'junegunn/goyo.vim'
Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
"Plug 'airblade/vim-gitgutter'
"Plug 'tomasiser/vim-code-dark'
"Plug 'tpope/vim-fugitive'
" Initialize plugin system with:PlugInstall
call plug#end()

"if $TERM == "rxvt-unicode-256color"
"  set t_Co=256
"endif

" Connect system clipboard, you might want to disable this option and keep
" seperate clipboards.
set clipboard=unnamedplus

syntax on
set number

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Always display the status line, even if only one window is displayed
set laststatus=2

" Instead of failing a command because of unsaved changes, instead raise a
" " dialogue asking if you wish to save changed files.
set confirm

" Do not save ~
set nobackup
set nowritebackup

" Default color scheme
set t_Co=256

map <leader>vimrc :tabe ~/.vim/.vimrc<cr>
autocmd bufwritepost .vimrc source $MYVIMRC


" Goyo
"map <C-g> :Goyo<CR>
"let g:goyo_linenr = 1
"
"function! s:goyo_enter()
"  silent !tmux set status off
"  silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
"  set noshowmode
"  set noshowcmd
"  set scrolloff=999
"  set number
"endfunction
"
"function! s:goyo_leave()
"  silent !tmux set status on
"  silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
"  set showmode
"  set showcmd
"  set scrolloff=5
"	source ~/.vimrc
"endfunction

"Reload when Goyo closed
"autocmd! User GoyoLeave
"autocmd  User GoyoLeave nested source ~/.vimrc

"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"
" " If you want :UltiSnipsEdit to split your window.
"let g:UltiSnipsEditSplit="vertical"

"colorscheme snow
colorscheme apprentice

" set wordwrap for markdown
au BufRead,BufNewFile *.md setlocal textwidth=80
