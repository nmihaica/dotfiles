# Reloads the bashrc file
alias bashreload="source ~/.bashrc && echo Bash config reloaded"
alias flushdns="sudo /etc/init.d/dns-clean restart && echo DNS cache flushed"

# Prints disk usage in human readable form
alias d="df -h"

alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias ll='ls -AlhF --color=auto'
alias la='ls -A'
alias l='ls -CF'

# Make some of the file manipulation programs verbose
alias mv="mv -v"
alias rm="rm -vi"
alias cp="cp -v"

# GREP Motifications
alias grep="grep --color"
alias grepp="grep -P --color"

# Json tools (pipe unformatted to these to nicely format the JSON)
alias json="python -m json.tool"
alias jsonf="python -m json.tool"

# Edit shortcuts for config files
alias sshconfig="${EDITOR} ~/.ssh/config"
alias bashrc="${EDITOR} +120 ~/.bashrc && source ~/.bashrc && echo Bash config edited and reloaded."

# SSH helper
alias sshlist="netstat -tnpa | grep 'ESTABLISHED.*sshd'"

# Clear the screen of your clutter
alias c="clear"
alias cl="clear;ls;pwd"

alias q="exit"
alias o="xdg-open"

alias local_server='python -m SimpleHTTPServer 8000'

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias notify='notify-send'
alias beep='play -n synth 0.1 sine 800 vol 0.4'

# Alias's for SSH
# alias SERVERNAME='ssh YOURWEBSITE.com -l USERNAME -p PORTNUMBERHERE'
# alias SERVERNAME='ssh YOURWEBSITE.com -l USERNAME -p PORTNUMBERHERE'
# alias SERVERNAME='ssh YOURWEBSITE.com -l USERNAME -p PORTNUMBERHERE'
# alias SERVERNAME='ssh YOURWEBSITE.com -l USERNAME -p PORTNUMBERHERE'


# Change directory aliases
alias home='cd ~'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# Structure
alias repos='cd ~/Repos && clear && ls -AlhF --color=auto && pwd'
alias documents='cd ~/Documents'
alias cloud='cd ~/Dropbox'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'
#alias projectname = 'cd ~/Repos/projectname'

# Search command line history
alias h="history | grep "
